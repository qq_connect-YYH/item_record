package com.code.common.vo;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Getter;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * @author Y.YH
 * @date 2020/11/21 11:27
 * @description
 */
@Getter
//@Setter
public class TimePair implements Serializable{


    /**
     * 开始日期(会随着促销日期往前补偿)
     */
    @JSONField(format = DatePattern.NORM_DATETIME_PATTERN)
    private Timestamp start;

    private int startYear;

    /**
     * 开始日期(原开始日期，不会变)
     */
    @JSONField(format = DatePattern.NORM_DATETIME_PATTERN)
    private Timestamp originStart;

    /**
     * 结束日期
     */
    @JSONField(format = DatePattern.NORM_DATETIME_PATTERN)
    private Timestamp end;

    private int endYear;

    /**
     * 销售平台
     */
    private String saleChannel;

    /**
     * 促销时间集合
     */
    private List<TimePair> specialSaleList;

    public TimePair(long start, long end) {
        this(start, end, "");
    }

    public TimePair(long start, long end, String saleChannel) {
        this(new Timestamp(start), new Timestamp(end), saleChannel);
    }

    public TimePair(Timestamp start, Timestamp end) {
        this(start, end, "");
    }

    public TimePair(Timestamp start, Timestamp end, String saleChannel) {
        this.start = start;
        this.end = end;
        this.saleChannel = saleChannel;
    }

    public void calcStartTime(long timeMillis) {
        if(this.originStart == null){
            this.originStart = new Timestamp(start.getTime());
        }
        long time = this.start.getTime() + timeMillis;
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(time);
        this.start = new Timestamp(instance.getTime().getTime());
        startYear = instance.get(Calendar.YEAR);
    }

    public Timestamp getOriginStart() {
        if(this.originStart == null){
            this.originStart = new Timestamp(start.getTime());
        }
        return originStart;
    }

    public int getStartYear(){
        if(startYear < 1900){
            Calendar instance = Calendar.getInstance();
            instance.setTime(start);
            startYear = instance.get(Calendar.YEAR);
        }
        return startYear;
    }

    public int getEndYear(){
        if(endYear < 1900){
            Calendar instance = Calendar.getInstance();
            instance.setTime(end);
            endYear = instance.get(Calendar.YEAR);
        }
        return endYear;
    }

    public long getStartTM(){
        return start.getTime();
    }

    public long getEndTM(){
        return end.getTime();
    }

    /**
     * 添加促销日期
     * @param tp
     */
    public void addSpecialSale(TimePair tp){
        if(specialSaleList == null){
            specialSaleList = new ArrayList<>(6);
        }
        specialSaleList.add(tp);

        //如果添加的促销日期开始时间已经存在，则取结束时间最大那个
//        boolean addOk = false;
//        for (int i = 0; i < specialSaleList.size(); i++) {
//            TimePair timePair = specialSaleList.get(i);
//            if(timePair.getStartTM() == tp.getStartTM()){
//                if(timePair.getEndTM() < tp.getEndTM()){
//                    specialSaleList.add(i, tp);
//                    addOk = true;
//                    break;
//                }
//            }
//        }
//        if(!addOk){
//            specialSaleList.add(tp);
//        }
    }

    public void addSpecialSale(Timestamp start, Timestamp end, String saleChannel){
        TimePair tp = new TimePair(start, end, saleChannel);
        this.addSpecialSale(tp);
    }

    public static Timestamp parseTimestamp(String time){
        long val = DateUtil.parse(time, DatePattern.NORM_DATETIME_PATTERN).getTime();
        return new Timestamp(val);
    }

}
