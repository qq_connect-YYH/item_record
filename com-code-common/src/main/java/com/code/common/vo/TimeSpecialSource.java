package com.code.common.vo;

import cn.hutool.core.date.DatePattern;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

/**
 * @author Y.YH
 * @date 2020/11/23 10:04
 * @description
 */
@Getter
@Setter
public class TimeSpecialSource {

    /**
     * 自定义促销代码
     */
    private String code;

    /**
     * 促销开始日期
     */
    @JSONField(format = DatePattern.NORM_DATETIME_PATTERN)
    private Timestamp startDate;

    /**
     * 促销结束日期
     */
    @JSONField(format = DatePattern.NORM_DATETIME_PATTERN)
    private Timestamp endDate;
    /**
     * 促销天数
     */
    private int interval;

    /**
     * 销售平台
     */
    private String saleChannel;
}
