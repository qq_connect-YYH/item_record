package com.code.common.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Description 验证字段是否符合条件
 * @Author y.yh
 * @Date 2020/09/29 11:26
 **/
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD })
public @interface CheckColumn {

    /**
     * 默认该字段必填
     *
     * @return
     */
    boolean required() default true;

    /**
     * 列名
     *
     * @return
     */
    String columnName() default "";

    /**
     * 字段最大长度验证，为0不校验
     *
     * @return
     */
    int maxLength() default 0;

    /**
     * 验证是否为正整数
     *
     * @return
     */
    boolean isPositiveInteger() default false;

    /**
     * 验证是否为数,含小数、整数
     *
     * @return
     */
    boolean isNumber() default false;

    /**
     * 最大值限制
     *
     * @return
     */
    int maxLimit() default -255;

    /**
     * 最小值限制
     *
     * @return
     */
    int minLimit() default -255;

}
