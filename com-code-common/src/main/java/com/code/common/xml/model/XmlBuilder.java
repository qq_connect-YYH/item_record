package com.code.common.xml.model;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class XmlBuilder {
    private static String XML_VERSION = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
    private Element root;

    public XmlBuilder() {
    }

    public void setRoot(Element root) {
        this.root = root;
    }

    public Element getRoot() {
        return root;
    }

    public String builder() {
        StringBuilder sb = new StringBuilder();
        sb.append(XML_VERSION);
        sb.append(root.startTag());
        List<Element> elements = root.getElements();
        if (elements != null && elements.size() > 0) {
            elements.forEach(element -> {
                recursiveElement(element, sb);
            });
        }
        sb.append(root.endTag());

        return sb.toString();
    }

    private void recursiveElement(Element element, StringBuilder sb) {
        List<Element> elements = element.getElements();
        if (CollectionUtils.isNotEmpty(elements)) {
            sb.append(element.startTag());
            elements.forEach(subElement -> {
                recursiveElement(subElement, sb);
            });
            sb.append(element.endTag());
        }
        else {
            String value = element.getValue();
            if(StringUtils.isNotBlank(value)){
                sb.append(element.startTag())
                    .append(StringUtils.isNotEmpty(value) ? StringEscapeUtils.escapeXml11(value) : "")
                    .append(element.endTag());
            }else{
                sb.append(element.startEmptyTag());
            }

        }
    }
}