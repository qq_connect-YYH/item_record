package com.code.common.xml.model;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Element {
    public static final String BLANK = " ";

    public static final String QUOTE = "\"";

    public static final String EQUALS = "=";

    List<Element> elements;

    Map<String, String> properties;

    public Element(String name) {
        this.name = name;
    }

    public Element(String name, String value) {
        this.name = name;
        this.value = value;
    }

    private String name;

    private String value;

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public List<Element> getElements() {
        return elements;
    }

    public Element create(String name) {
        return create(name, null);
    }

    public Element create(String name, String value) {
        if (elements == null) {
            elements = new ArrayList<Element>();
        }

        Element element = new Element(name, value);
        elements.add(element);
        return element;
    }

    public Element addAttr(String key, String value) {
        if (properties == null) {
            // 大部分情况Element的属性不会超过2个
            properties = new LinkedHashMap<String, String>(2);
        }

        properties.put(key, value);

        return this;
    }

    public String startEmptyTag() {
        if (properties == null || properties.isEmpty()) {
            return "<" + name + "/>";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("<" + name).append(Element.BLANK);
        for (Entry<String, String> entry : properties.entrySet()) {
            sb.append(entry.getKey()).append(Element.EQUALS).append(Element.QUOTE).append(entry.getValue())
                    .append(QUOTE).append(BLANK);
        }
        // 删除最后一个BLANK
        sb.deleteCharAt(sb.length() - 1);
        sb.append("/>");
        return sb.toString();
    }
    public String startTag() {
        if (properties == null || properties.isEmpty()) {
            return "<" + name + ">";
        }

        StringBuilder sb = new StringBuilder();
        sb.append("<" + name).append(Element.BLANK);
        for (Entry<String, String> entry : properties.entrySet()) {
            sb.append(entry.getKey()).append(Element.EQUALS).append(Element.QUOTE).append(entry.getValue())
                    .append(QUOTE).append(BLANK);
        }
        // 删除最后一个BLANK
        sb.deleteCharAt(sb.length() - 1);
        sb.append(">");
        return sb.toString();
    }

    public String endTag() {
        return "</" + name + ">";
    }

    @Override
    public String toString() {
        return name + (StringUtils.isNotEmpty(value) ? ": " + value : "");
    }
}