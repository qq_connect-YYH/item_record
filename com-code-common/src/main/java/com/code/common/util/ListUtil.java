package com.code.common.util;

import org.apache.commons.collections.CollectionUtils;

import java.util.List;
import java.util.function.Consumer;

/**
 * @author Y.YH
 * @date 2020/7/31 10:30
 * @description
 */
public class ListUtil {

    /**
     * 集合批处理
     * @param list
     * @param batchSize
     * @param consumer
     * @param <E>
     */
    public static <E> void batchResolve(List<E> list, int batchSize, Consumer<List<E>> consumer) {
        if (CollectionUtils.isEmpty(list) || batchSize <= 0 || consumer == null) {
            return;
        }

        int size = list.size();
        if (size <= batchSize) {
            consumer.accept(list);
            return;
        }

        int page = (size - 1) / batchSize + 1;
        int start = 0, end = 0;
        for (int i = 1; i <= page; i++) {
            end = start + (i == page ? (size - batchSize * (i - 1)) : batchSize);
            consumer.accept(list.subList(start, end));
            start = end;
        }
    }

}
