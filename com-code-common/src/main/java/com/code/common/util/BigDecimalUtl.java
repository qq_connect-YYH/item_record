package com.code.common.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by Administrator on 2020/9/19.
 */
public class BigDecimalUtl {

    public static double format(double val){
        BigDecimal bigDecimal = new BigDecimal(val);
        double v = bigDecimal.setScale(2, RoundingMode.HALF_UP).doubleValue();
        return v;
    }
}
