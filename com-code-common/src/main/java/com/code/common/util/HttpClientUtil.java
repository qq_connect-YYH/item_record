package com.code.common.util;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Map;

/**
 * @author Y.YH
 * @date 2020/8/7 10:42
 * @description
 */
@Slf4j
public class HttpClientUtil {

    public static InputStream doGetStream(String url){
        try {
            HttpGet httpGet = new HttpGet(url);
            HttpClient httpClient = HttpClientBuilder.create().build();
            HttpResponse response = httpClient.execute(httpGet);
            HttpEntity entity = response.getEntity();
            return entity.getContent();
        }catch (Exception e){
            log.error("stream error ", e);
        }
        return null;
    }

    public static Object doGet(String url){
        return doGet(url, null, null);
    }

    public static Object doGet(String url, Map<String, Object> param, Map<String, Object> headers){
        try {
            //设置参数
            if(param != null && !param.isEmpty()){
                StringBuilder str = new StringBuilder("?");
                param.forEach((k,v) -> {
                    str.append(String.format("%s=%s&", k, v));
                });
                str.replace(str.length()-1, str.length(), "");
                url = url + str;
            }

            HttpGet httpGet = new HttpGet(url);
            //设置请求头
            if(headers != null && !headers.isEmpty()){
                headers.forEach((k,v) -> {
                    httpGet.setHeader(k, v.toString());
                });
            }

            HttpClient httpClient = HttpClientBuilder.create().build();
            HttpResponse response = httpClient.execute(httpGet);
            HttpEntity entity = response.getEntity();
            String responseJson = EntityUtils.toString(entity);
            if (response.getStatusLine().getStatusCode() == 200 && entity != null) {
                Object val = JSON.parseObject(responseJson, Object.class);
//                log.info("val {}", val);
                return val;
            }else{
//                log.info("error {}", response);
            }
            return responseJson;
        }catch (Exception e){
            log.error("", e);
        }
        return null;
    }

    public static Object doPost(String url){
        return doPost(url, null, null);
    }
    public static Object doPost(String url, Map<String, Object> reqBody, Map<String, Object> headers){
        HttpPost httpPost = new HttpPost(url);
        //设置请求头
        if(headers != null && !headers.isEmpty()){
            headers.forEach((k,v) -> {
                httpPost.setHeader(k, v.toString());
            });
        }
        httpPost.addHeader("Content-type", "application/json; charset=utf-8");
        try {
            //设置请求体
            if(reqBody != null && !reqBody.isEmpty()) {
//                StringEntity stringEntity = new StringEntity(JSON.toJSONString(reqBody), Charset.forName("utf-8"));
                StringEntity stringEntity = new StringEntity(JSON.toJSONString(reqBody), ContentType.create(ContentType.APPLICATION_JSON.getMimeType(), "utf-8"));
                // 把表单对象设置到httpPost中
                httpPost.setEntity(stringEntity);
            }

            // 使用HttpClient发起请求，返回response
            HttpClient httpClient = HttpClientBuilder.create().build();
            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity entity = response.getEntity();
            String responseJson = EntityUtils.toString(entity);

            return responseJson;
        } catch (Exception e) {
            log.error("post error :", e);
        }

        return null;
    }

    public static Object doPostFile(String url, Map<String, Object> reqBody, Map<String, Object> headers, File file){
        HttpPost httpPost = new HttpPost(url);
        //设置请求头
        if(headers != null && !headers.isEmpty()){
            headers.forEach((k,v) -> {
                httpPost.setHeader(k, v.toString());
            });
        }
        ContentType strContent = ContentType.create("text/plain", Charset.forName("UTF-8"));
        try {
            MultipartEntityBuilder meb = MultipartEntityBuilder.create();
            meb.addBinaryBody("file", file);
            //设置请求体
            if(reqBody != null && !reqBody.isEmpty()) {
                reqBody.forEach((k,v) ->{
                    meb.addTextBody(k, v.toString(), strContent);
                });
            }
            HttpEntity httpEntity = meb.build();
            httpPost.setEntity(httpEntity);

            // 使用HttpClient发起请求，返回response
            HttpClient httpClient = HttpClientBuilder.create().build();
            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity entity = response.getEntity();
            String responseJson = EntityUtils.toString(entity);

            return responseJson;
        } catch (Exception e) {
            log.error("post error :", e);
        }

        return null;
    }
}
