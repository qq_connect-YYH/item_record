package com.code.common.util;

import com.code.common.constant.SpecialTimeConstant;
import com.code.common.vo.TimePair;
import com.code.common.vo.TimeSpecialSource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Y.YH
 * @date 2020/11/23 10:33
 * @description
 */
@Slf4j
public class TimePairUtil {


    /**
     *
     * 注：
     1. 平台下的促销日期不能重叠，不然会有重复促销日期
     2. 计算后的开始日期，理论上不会又与促销日期重叠
     3. 日期最好在1年内
     * @param timePair
     */
    public static void calcTimePair(TimePair timePair) {
        calcTimePair(timePair, true);
    }
    /**
     * @param timePair
     * @param makeUpTime true：补偿时间，false：不补偿时间
     */
    public static void calcTimePair(TimePair timePair, boolean makeUpTime) {
        //根据查询的开始年份，生成去年、今年的所有促销日期
        int startYearCalc = timePair.getStartYear() - 1;
        List<TimeSpecialSource> timeSpecialSources = new ArrayList<>(8);
        do {
            for (SpecialTimeConstant.SpecialTimeEnum anEnum : SpecialTimeConstant.specialTimeEnums) {
                TimeSpecialSource bean = new TimeSpecialSource();
                bean.setCode(anEnum.getCode());
                bean.setInterval(anEnum.getInterval());
                bean.setSaleChannel(anEnum.getSaleChannel());
                Timestamp dateStart = TimePair.parseTimestamp(startYearCalc + "-" + anEnum.getStartDate());
                bean.setStartDate(dateStart);
                Timestamp dateEnd =  TimePair.parseTimestamp(startYearCalc + "-" + anEnum.getEndDate());
                bean.setEndDate(dateEnd);
                timeSpecialSources.add(bean);
            }
        }while (++startYearCalc <= timePair.getEndYear());
        //按开始日期降序
        timeSpecialSources.sort( (o1, o2) -> o1.getStartDate().getTime() > o2.getStartDate().getTime() ? -1 : 1);

        for (TimeSpecialSource sTime : timeSpecialSources) {
            if(StringUtils.isNotBlank(timePair.getSaleChannel())
                    && StringUtils.isNotBlank(sTime.getSaleChannel())
                    && !timePair.getSaleChannel().equalsIgnoreCase(sTime.getSaleChannel())){
                continue;
            }
            if(makeUpTime){
                calcTime(timePair, sTime.getSaleChannel(), sTime.getStartDate(), sTime.getEndDate());
            }else{
                calcTimeNonMakeUp(timePair, sTime.getSaleChannel(), sTime.getStartDate(), sTime.getEndDate());
            }
        }
    }

    /**
     * 查询开始日期 会根据促销日期时间多少 往前补偿时间
     * @param timePair
     * @param saleChannel
     * @param dateStart
     * @param dateEnd
     */
    public static void calcTime(TimePair timePair, String saleChannel, Timestamp dateStart, Timestamp dateEnd) {
//        log.info("");
//        log.info(String.format("查询时间 %s ~ %s", timePair.getStart(), timePair.getEnd()));
//        log.info(String.format("促销时间 %s ~ %s", dateStart, dateEnd));
        // 在促销期之外 结束当前循环 (等号待确认？)
        if(timePair.getEndTM() <= dateStart.getTime() || timePair.getStartTM() >= dateEnd.getTime()){
            return;
        }

        //1. 中间包含促销,边界不重合  (..., 促销, ...)
        if(timePair.getStartTM() < dateStart.getTime() && timePair.getEndTM() > dateEnd.getTime()){
            //添加促销时间 (促销开始时间, 促销结束时间)
            timePair.addSpecialSale(dateStart, dateEnd, saleChannel);
            //start 往前补时间
            //促销结束时间 - 促销开始时间
            long increment = -(dateEnd.getTime() - dateStart.getTime());
            timePair.calcStartTime(increment);
        }
        //2. end包含 ( ..., 促销]
        else if(timePair.getStartTM() < dateStart.getTime() && timePair.getEndTM() <= dateEnd.getTime()){
            //添加促销时间 (促销开始时间, 查询结束时间)
            timePair.addSpecialSale(dateStart, timePair.getEnd(), saleChannel);
            //start 往前补时间
            // 查询结束时间 - 促销开始时间
            long increment = -(timePair.getEnd().getTime() - dateStart.getTime());
            timePair.calcStartTime(increment);
        }
        //3. 被促销包含 [促销, ..., 促销]
        else if(timePair.getStartTM() >= dateStart.getTime() && timePair.getEndTM() <= dateEnd.getTime() ){
            //添加促销时间 (促销开始时间, 查询结束时间)
            timePair.addSpecialSale(dateStart, timePair.getEnd(), saleChannel);
            //start 往前补时间
            // 查询结束时间 - 促销开始时间
            long increment = -(timePair.getEnd().getTime() - dateStart.getTime());
            timePair.calcStartTime(increment);
        }
        //4. start包含 [促销, ...)
        else if(timePair.getStartTM() >= dateStart.getTime() && timePair.getEndTM() > dateEnd.getTime()){
            //添加促销时间 (促销开始时间, 促销结束时间)
            timePair.addSpecialSale(dateStart, dateEnd, saleChannel);
            //start 往前补时间
            //(查询开始时间 - 促销开始时间) + (促销结束时间 - 查询开始时间)
            long increment = -(Math.abs(timePair.getStartTM() - dateStart.getTime() + Math.abs(dateEnd.getTime() - timePair.getStartTM()))) ;
            timePair.calcStartTime(increment);
        }
    }

    /**
     * 1.查询开始日期 不会往前补偿时间
     * 2. 如果时间与促销日期是尾部包含 [开始时间,...)  ，促销日期也不会补偿时间（注意这是不同点）
     * @param timePair
     * @param saleChannel
     * @param dateStart
     * @param dateEnd
     */
    private static void calcTimeNonMakeUp(TimePair timePair, String saleChannel, Timestamp dateStart, Timestamp dateEnd) {
        // 在促销期之外 结束当前循环 (等号待确认？)
        if(timePair.getEndTM() <= dateStart.getTime() || timePair.getStartTM() >= dateEnd.getTime()){
            return;
        }

        //1. 中间包含促销,边界不重合  (..., 促销, ...)
        if(timePair.getStartTM() < dateStart.getTime() && timePair.getEndTM() > dateEnd.getTime()){
            //添加促销时间 (促销开始时间, 促销结束时间)
            timePair.addSpecialSale(dateStart, dateEnd, saleChannel);
        }
        //2. end包含 ( ..., 促销]
        else if(timePair.getStartTM() < dateStart.getTime() && timePair.getEndTM() <= dateEnd.getTime()){
            //添加促销时间 (促销开始时间, 查询结束时间)
            timePair.addSpecialSale(dateStart, timePair.getEnd(), saleChannel);
        }
        //3. 被促销包含 [促销, ..., 促销]
        else if(timePair.getStartTM() >= dateStart.getTime() && timePair.getEndTM() <= dateEnd.getTime() ){
            //添加促销时间 (查询开始时间, 查询结束时间)
            timePair.addSpecialSale(timePair.getStart(), timePair.getEnd(), saleChannel);
        }
        //4. start包含 [促销, ...)
        else if(timePair.getStartTM() >= dateStart.getTime() && timePair.getEndTM() > dateEnd.getTime()){
            //添加促销时间 (查询开始时间, 促销结束时间)
            timePair.addSpecialSale(timePair.getStart(), dateEnd, saleChannel);
        }
    }
}
