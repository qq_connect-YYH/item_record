package com.code.common.util;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

/**
 * @author Y.YH
 * @date 2020/1/11
 * @description
 */
public class SystemUtils {

    /**
     *  根据系统获取服务器的ip地址 InetAddress inet = InetAddress.getLocalHost(); 但是上述代码在Linux下返回127.0.0.1。
     *  主要是在linux下返回的是/etc/hosts中配置的localhost的ip地址，
     *  而不是网卡的绑定地址。改用网卡的绑定地址，可以取到本机的ip地址。
     */
    public static String getCurrentSystemIp() {
        try {
            if (isWindowOS()) {
                return InetAddress.getLocalHost().getHostAddress();
            }

            InetAddress inetAddress = null;
            boolean bFindIP = false;
            // 定义一个内容都是NetworkInterface的枚举对象
            Enumeration<NetworkInterface> netInterfaces = NetworkInterface.getNetworkInterfaces();
            while (netInterfaces.hasMoreElements()) {
                if (bFindIP) {
                    break;
                }

                NetworkInterface ni = (NetworkInterface) netInterfaces.nextElement();
                // 遍历所有IP
                Enumeration<InetAddress> ips = ni.getInetAddresses();
                while (ips.hasMoreElements()) {
                    inetAddress = (InetAddress) ips.nextElement();
                    if (inetAddress.isSiteLocalAddress() // 属于本地地址
                            && !inetAddress.isLoopbackAddress() // 不是回环地址
                            && inetAddress.getHostAddress().indexOf(":") == -1) { // 地址里面没有:号
                        bFindIP = true; // 找到了地址
                        break;
                    }
                }
            }

            return inetAddress.getHostAddress();
        }
        catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * @Description: 是否为window系统
     */
    public static boolean isWindowOS() {
        String osName = System.getProperty("os.name");
        if (osName.toLowerCase().indexOf("windows") > -1) {
            return true;
        }
        return false;
    }

}
