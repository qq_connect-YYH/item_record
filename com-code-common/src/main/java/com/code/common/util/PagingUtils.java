package com.code.common.util;

import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Y.YH
 * @date 2020/7/31 10:31
 * @description
 */
public class PagingUtils {

    /**
     * 分页数据
     * @param datas
     * @param pageNo
     * @param pageSize
     * @return
     */
    public static <T> List<T> paging(List<T> datas, int pageNo, int pageSize) {
        if (CollectionUtils.isEmpty(datas) || datas.size() <= pageSize) {
            return datas;
        }

        int fromIndex = (pageNo - 1) * pageSize;
        if (fromIndex < 0) {
            fromIndex = 0;
        }

        int size = datas.size();
        int toIndex = pageNo * pageSize;

        if (toIndex > size) {
            toIndex = size;
        }

        if (fromIndex >= toIndex) {
            return null;
        }

        return datas.subList(fromIndex, toIndex);
    }

    /**
     * @param datas
     * @param pageSize
     * @return
     */
    public static <T> List<List<T>> pagingList(List<T> datas, int pageSize) {
        List<List<T>> pagingList = new ArrayList<>();
        if (CollectionUtils.isEmpty(datas) || datas.size() <= pageSize) {
            pagingList.add(datas);
            return pagingList;
        }

        int total = datas.size();
        int totalPage = (total + pageSize - 1) / pageSize;
        for (int no = 1; no <= totalPage; no++) {
            List<T> paging = paging(datas, no, pageSize);

            pagingList.add(paging);
        }
        return pagingList;
    }
}
