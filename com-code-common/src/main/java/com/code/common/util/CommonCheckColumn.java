package com.code.common.util;

import com.code.common.annotation.CheckColumn;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.FieldUtils;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.List;

/**
 * @Description 公共验证对象属性
 * @Author y.yh
 * @Date 2020/09/29 11:26
 **/
public class CommonCheckColumn {

    /**
     * 验证对象有@CheckColumn注解的属性，是否合格
     *
     * @param t
     * @param <T>
     * @return true:合格，false:不合格
     */
    public static <T> boolean checkFieldReturnBoolean(T t) {
        String msg = checkFieldReturnMsg(t);
        if (StringUtils.isEmpty(msg)) {
            return true;
        }
        return false;
    }

    /**
     * 验证对象有@CheckColumn注解的属性
     *
     * @param t
     * @param <T>
     * @return 返回不合格的消息
     */
    public static <T> String checkFieldReturnMsg(T t) {
        if (t == null) {
            return null;
        }
        Class clazz = t.getClass();
        List<Field> fields = FieldUtils.getAllFieldsList(clazz);
        CheckColumn checkColumn;
        StringBuilder result = new StringBuilder();
        for (Field field : fields) {
            field.setAccessible(true);
            checkColumn = field.getAnnotation(CheckColumn.class);
            if (checkColumn != null) {
                String msg = checkColumn(t, field, checkColumn);
                if (StringUtils.isNotBlank(msg)) {
                    result.append(String.format("【%s：%s】", checkColumn.columnName(), msg));
                }
            }
        }
        return result.toString();
    }

    /**
     * 验证字段是否满足条件,如果第一个校验不满足，直接返回该提示
     *
     * @param field
     * @param checkColumn
     * @return
     */
    private static <T> String checkColumn(T t, Field field, CheckColumn checkColumn) {
        // 所有值用字符串代表
        Object value;
        try {
            value = field.get(t);
            if (value == null) {
                value = "";
            }
        } catch (IllegalAccessException e) {
            value = "";
        }
        String valid = NumericUtils.formatObjectToStr(value);
        boolean required = checkColumn.required();
        // 非空
        if (StringUtils.isBlank(valid)) {
            if (required) {
                return "不能为空";
            } else {
                return valid;
            }
        }
        // 最大长度
        int maxLength = checkColumn.maxLength();
        if (maxLength > 0) {
            if (valid.length() > maxLength) {
                return String.format("最大长度为%s", maxLength);
            }
        }
        // 是否为正整数
        boolean isPositiveInteger = checkColumn.isPositiveInteger();
        if (isPositiveInteger) {
            boolean numeric = StringUtils.isNumeric(valid);
            if (!numeric) {
                return String.format("必须为正整数");
            }
        }
        // 一个数
        boolean isNumber = checkColumn.isNumber();
        if (isNumber) {
            boolean isNum = valid.matches("^(\\-|\\+)?\\d+(\\.\\d+)?$");
            if (!isNum) {
                return String.format("必须为小数或整数");
            }
        }
        // 最大值限制
        int maxLimit = checkColumn.maxLimit();
        if (maxLimit != -255) {
            try {
                if (new BigDecimal(valid).compareTo(new BigDecimal(maxLimit)) > 0) {
                    return String.format("（值：%s）> %s", valid, maxLimit);
                }
            }catch (Exception e){
                return String.format("值：%s，最大值验证失败，转换异常：%s", valid, e.getMessage());
            }
        }
        // 最小值限制
        int minLimit = checkColumn.minLimit();
        if (minLimit != -255) {
            try {
                if (new BigDecimal(valid).compareTo(new BigDecimal(minLimit)) < 0) {
                    return String.format("（值：%s）< %s", valid, minLimit);
                }
            }catch (Exception e){
                return String.format("值：%s，最小值验证失败，转换异常：%s", valid, e.getMessage());
            }
        }

        return "";
    }

}
