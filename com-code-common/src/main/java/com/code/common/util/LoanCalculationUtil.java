package com.code.common.util;

/**
 * Created by Administrator on 2020/9/19.
 * 贷款计算
 */
public class LoanCalculationUtil {

    /**
     * 等额本金 每月还款额
     * @param principal 本金
     * @param monthlyRate 每月利率
     * @param numberOfPeriods 还款期数
     * @return
     */
    public static double equalPrincipal(double principal, double monthlyRate, int numberOfPeriods){
        /*
        计算公式：每月还款额 = 贷款本金 / 贷款期数 + (本金 - 已归还本金累计额) * 月利率
         */
        //每期还款本金
        double benJinMonth = principal / numberOfPeriods;
        //当期期还款利息
        double firstMonthInterest = principal * monthlyRate;

        return benJinMonth + firstMonthInterest;
//        return benJinMonth + (firstMonthInterest/2 + lastMonthInterest/2) * 12;
//        return (firstMonthInterest + lastMonthInterest) * 6 ;
    }

    /**
     * 等额本息 每月还款额
     * @param principal 本金
     * @param monthlyRate   每月利率
     * @param numberOfPeriods   还款期数
     * @return
     */
    public static double equalPrincipalAndInterest(double principal, double monthlyRate, int numberOfPeriods){
        /*
        等额本息，是指在还款期内，每月偿还同等数额du的贷款(包括本金和利息)。
        等额本息还款法即借款人每月按相等的金额偿还贷款本息，其中每月贷款利息按月初剩余贷款本金计算并逐月结清。
        每月应还款数额计算公式：
        每月还款额=[贷款本金×月利率×（1+月利率）^还款月数]÷[（1+月利率）^还款月数－1]
         */


        double money = (principal * monthlyRate * Math.pow((1+monthlyRate), numberOfPeriods)) / (Math.pow((1+monthlyRate), numberOfPeriods) - 1);

        return money;
    }


}
