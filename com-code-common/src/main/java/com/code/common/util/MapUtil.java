package com.code.common.util;

import java.util.Map;
import java.util.function.Supplier;

/**
 * @author Y.YH
 * @date 2019/12/6 18:10
 * @description
 */
public class MapUtil {

    /**
     *
     * @Description: 线程安全的 putIfAbsent方法, 仅针对线程安全的map。
     * 如果多线程同时传入key对应的value已经存在，就返回存在的value，不进行替换。如果不存在，就添加key和value，返回null
     */
    public static <K, V> V putIfAbsent(Map<K, V> map, K key, Supplier<V> supplier) {
        V v = map.get(key);
        if (v == null) {
            V value = supplier.get();
            //线程安全的 putIfAbsent方法
            v = map.putIfAbsent(key, value);
            if (v == null) {
                v = value;
            }
        }

        return v;
    }
}
