package com.code.common.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

/**
 * 线程池工具类
 */
@Slf4j
public class ExecutorUtils {
    private static final Timer TIMER = new Timer();

    private static final Map<String, ThreadPoolExecutor> THREAD_POOL_MAP = new HashMap<>();

    static {
        TIMER.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                System.out.println("------->scan thread pool current tasks info<-------");
                for (Entry<String, ThreadPoolExecutor> entry : THREAD_POOL_MAP.entrySet()) {
                    String poolName = entry.getKey();
                    ThreadPoolExecutor pool = entry.getValue();
                    log.warn("{} poolSize: {}, queueSize: {}, activeCount: {}, taskCount: {}, completedTaskCount: {}.",
                            poolName, pool.getPoolSize(), pool.getQueue().size(), pool.getActiveCount(), pool.getTaskCount(),  pool.getCompletedTaskCount());
                }
            }
        }, 60 * 1000, 2 * 60 * 1000);
    }

    /**
     * 创建默认线程池，线程回收时间1分钟
     * @param size 大小
     * @return pool
     */
    public static ThreadPoolExecutor newFixedThreadPool(String poolName, int size) {
        return newFixedThreadPool(poolName, size, 60000);
    }

    /**
     * 创建线程池
     * 
     * @param poolName 线程池名称
     * @param size 大小
     * @param keepAliveTime 线程回收时间
     * @return
     */
    public static ThreadPoolExecutor newFixedThreadPool(String poolName, int size, long keepAliveTime) {
        ThreadPoolExecutor pool = new ThreadPoolExecutor(size, size, keepAliveTime, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
        if (keepAliveTime > 0) {
            pool.allowCoreThreadTimeOut(true);
        }
        THREAD_POOL_MAP.put(poolName, pool);
        return pool;
    }

    public static void execute(ExecutorService pool, Runnable runnable, String threadSuffix) {
        //可以获取授权信息
        pool.execute(() -> {
            try {
                setThreadSuffix(threadSuffix);
                //设置授权信息
                runnable.run();
            }
            catch (Exception e) {

            }
        });
    }

    public static Future<Object> submit(ExecutorService pool, Consumer<Object> consumer, String threadSuffix) {
        //可以获取授权信息
        return pool.submit(() -> {
            Object obj = new Object();
            try {
                setThreadSuffix(threadSuffix);
                //设置授权信息
                consumer.accept(obj);
            }
            catch (Exception e) {
            }
            return obj;
        });
    }

    public static void setThreadSuffix(String threadSuffix) {
        if (StringUtils.isNotEmpty(threadSuffix)) {
            Thread thread = Thread.currentThread();
            String name = thread.getName();
            String join = "-->";
            if (name.contains(join)) {
                thread.setName(name.substring(0, name.indexOf(join)) + join + threadSuffix);
            }
            else {
                thread.setName(name + join + threadSuffix);
            }
        }
    }

    public static void main(String[] args) {
        newFixedThreadPool("test", 2);
    }
}
