package com.code.common.util;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Y.YH
 * @date 2020/7/30 11:27
 * @description
 */
public class CalendarUtil {



    /**
     * 获取n天前、后的日期
     * @param nday
     * @return
     */
    public static Calendar obtainNDay(int nday){
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_YEAR, nday);
        return c;
    }

    /**
     * 获取今天整天区间
     * @return
     */
    public static List<Date> obtainTodayTimeIntervalList() {
        List<Date> result = new ArrayList<>(2);
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        result.add(c.getTime());

        c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
        c.set(Calendar.MILLISECOND, 999);
        result.add(c.getTime());

        return result;
    }
    /**
     * 获取每天的开始时间、结束时间
     * @param startTime
     * @param endTime
     * @return
     */
    public static List<Map<String, Timestamp>> obtainTimeIntervalList(Timestamp startTime, Timestamp endTime) {
        Calendar tempStart = Calendar.getInstance();
        tempStart.setTime(startTime);

        Calendar tempEnd = Calendar.getInstance();
        tempEnd.setTime(endTime);
        tempEnd.set(Calendar.HOUR_OF_DAY, 0);
        tempEnd.set(Calendar.MINUTE, 0);
        tempEnd.set(Calendar.SECOND, 0);
        tempEnd.set(Calendar.MILLISECOND, 0);


        List<Map<String, Timestamp>> result = new ArrayList<>();
        //左闭右开区间 [startTime, endTime)
        while (tempStart.before(tempEnd)) {
            Map<String, Timestamp> map = new HashMap<>(2);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(tempStart.getTime());

            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            map.put("start", new Timestamp(calendar.getTimeInMillis()));

            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            calendar.set(Calendar.MILLISECOND, 999);
            map.put("end", new Timestamp(calendar.getTimeInMillis()));

            result.add(map);
            tempStart.add(Calendar.DAY_OF_YEAR, 1);
        }
        return result;
    }

    public static Date timeToDate(String time){
        Time time1 = Time.valueOf(time);
        Calendar c2 = Calendar.getInstance();
        c2.setTime(time1);

        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, c2.get(Calendar.HOUR_OF_DAY));
        c.set(Calendar.MINUTE, c2.get(Calendar.MINUTE));
        c.set(Calendar.SECOND, c2.get(Calendar.SECOND));
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }
}
