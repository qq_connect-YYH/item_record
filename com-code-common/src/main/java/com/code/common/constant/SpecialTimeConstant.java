package com.code.common.constant;


import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 促销
 */
@Getter
@Setter
public class SpecialTimeConstant {

    public static final String DOUBLE11_START_DATE = "11-11 00:00:00";  //"11-07 00:00:00";
    public static final String DOUBLE11_END_DATE = "11-12 00:00:00" ;
    public static final Integer DOUBLE11_INTERVAL = 1;

    @Getter
    public enum SpecialTimeEnum{
        DOUBLE_11_DISCOUNT("DOUBLE11", SpecialTimeConstant.DOUBLE11_START_DATE, SpecialTimeConstant.DOUBLE11_END_DATE,SpecialTimeConstant.DOUBLE11_INTERVAL,null),
        ;

        SpecialTimeEnum(String code, String startDate, String endDate, int interval, String saleChannel) {
            this.code = code;
            this.startDate = startDate;
            this.endDate = endDate;
            this.interval = interval;
            this.saleChannel = saleChannel;
        }

        /**
         * 自定义促销代码
         */
        private String code;

        /**
         *
         */
        private String startDate;

        /**
         * 异常提示信息
         */
        private String endDate;
        /**
         * 促销时间
         */
        private int interval;

        /**
         * 销售平台
         */
        private String saleChannel;
    }

    public static List<SpecialTimeEnum> specialTimeEnums = new ArrayList<>();
    static {
        specialTimeEnums.add(SpecialTimeEnum.DOUBLE_11_DISCOUNT);
    }
}
