package com.code.common.util;

import com.alibaba.fastjson.JSON;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ThreadPoolExecutor;

import static org.junit.Assert.assertEquals;

/**
 * @author Y.YH
 * @date 2020/1/2 11:54
 * @description
 */
public class TestFileChannel {

    public ThreadPoolExecutor executor = ExecutorUtils.newFixedThreadPool("syncListingActive", 5);
    public ConcurrentLinkedQueue<String> waitOutList = new ConcurrentLinkedQueue<>();

    @Test
    public void asyncFileout() throws InterruptedException {
        //并发输出有问题
        for (int  n= 0;  n< 5; n++) {
            executor.execute(() ->{
                for (int i = 0; i < 20; i++) {
                    waitOutList.add(i+" " + Thread.currentThread().getName());
                }
                File file = new File("d://successAccount.txt");
                try {
                    if(!file.exists()){
                        file.createNewFile();
                    }
                    RandomAccessFile writer = new RandomAccessFile(file, "rw");
                    FileChannel channel = writer.getChannel();
                    StringBuilder str = new StringBuilder();
                    while (waitOutList.size() >0){
                        String poll = waitOutList.poll();
                        str.append(poll +"\n");
                    }
                    ByteBuffer buff = ByteBuffer.wrap(str.toString().getBytes(StandardCharsets.UTF_8));
                    channel.write(buff);
                    writer.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            });
        }

        Thread.currentThread().join();
    }
    @Test
    public void fineChannel() {
        try{
            String file = TestFileChannel.class.getClassLoader().getResource("test.txt").getFile();
            RandomAccessFile reader = new RandomAccessFile(file, "r");
            FileChannel channel = reader.getChannel();
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            int bufferSize = 1024;
            if (bufferSize > channel.size()) {
                bufferSize = (int) channel.size();
            }
            ByteBuffer buff = ByteBuffer.allocate(bufferSize);

            while (channel.read(buff) > 0) {
                out.write(buff.array(), 0, buff.position());
                buff.clear();
            }

            String fileContent = new String(out.toByteArray(), StandardCharsets.UTF_8);

            assertEquals("Hello world", fileContent);

        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void whenWriteWithFileChannelUsingRandomAccessFile_thenCorrect()
            throws IOException {
        String file = "d://test.txt";
        try (RandomAccessFile writer = new RandomAccessFile(file, "rw");
             FileChannel channel = writer.getChannel()){
            ByteBuffer buff = ByteBuffer.wrap("Hello world".getBytes(StandardCharsets.UTF_8));

            channel.write(buff);

            // verify
            RandomAccessFile reader = new RandomAccessFile(file, "r");
            assertEquals("Hello world", reader.readLine());
            reader.close();
        }
    }

    @Test
    public void test() throws IOException {
        File test = File.createTempFile("test", ".png", new File("d://"));
        FileOutputStream fileOutputStream = new FileOutputStream(test);

        File file = new File("d://youdu图片20191114090857.png");
        System.out.println(String.format("%s 大小 %s", file.getName(), file.length()));
        FileInputStream fileInputStream = new FileInputStream(file);

        byte[] redBuffer = new byte[1024];
        while ( (fileInputStream.read(redBuffer)) != -1){
            fileOutputStream.write(redBuffer);
        }
        fileOutputStream.flush();
    }

    @Test
    public void testImageIo() throws IOException {
        File file = new File("d://youdu图片20191114090857.png");
        System.out.println(String.format("%s 大小 %s", file.getName(), file.length()));
        FileInputStream fileInputStream = new FileInputStream(file);

        BufferedImage read = ImageIO.read(fileInputStream);
        System.out.println(String.format("原图片的宽：%s,高：%s", read.getWidth(), read.getHeight()));

        // 图像的仿射变换——翻转（Flip）、旋转（Rotation）、平移（Translation）、缩放（Scale）和错切（Shear）
        AffineTransform scaleInstance = AffineTransform.getScaleInstance(0.5, 0.5);
        AffineTransformOp ato = new AffineTransformOp(scaleInstance, null);
        BufferedImage newImageRead = ato.filter(read, null);
        System.out.println(String.format("修改后图片的宽：%s,高：%s", newImageRead.getWidth(), newImageRead.getHeight()));


        File test = File.createTempFile("test", ".png", new File("d://"));
//        ImageIO.write(newImageRead, "png", test);

        FileOutputStream fileOutputStream = new FileOutputStream(test);
        ImageIO.write(newImageRead, "png", fileOutputStream);
        fileOutputStream.flush();

    }

    @Test
    public void testImg()   {
        try {
            String str = "https://imgsa.baidu.com/exp/w=480/sign=1699c1ad6663f6241c5d380bb745eb32/ac345982b2b7d0a2e8724e09c6ef76094a369aef.jpg";
            URL url = new URL(str);
            InputStream inputStream = url.openStream();

            File image = File.createTempFile("image", ".png", new File("d://"));
            FileOutputStream fileOutputStream = new FileOutputStream(image);
            byte[] buffer = new byte[1024];
            int len ;
            while ((len = inputStream.read(buffer)) != -1){
                fileOutputStream.write(buffer, 0, len);
            }
            fileOutputStream.flush();
            fileOutputStream.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void filterListingOK() throws IOException {
        File templateIds = new File("D:\\test\\aaa.txt");
        List<String> idsLine = FileUtils.readLines(templateIds);

//        File file = File.createTempFile("ListingOK_", ".txt", new File("D:\\Estone\\LAZADA\\com-item-codegenerate\\src\\main\\resources\\pmsale"));
        File file = new File("D:\\test\\xxx.txt");
        if(file.exists()){
            file.delete();
            file.createNewFile();
        }
        FileOutputStream fileOutputStream = new FileOutputStream(file);

        List<String> okUrls = new ArrayList<String>();


            //输出到文件
            if(okUrls.size() > 0){
                String str = String.format("【%s】", "test") + JSON.toJSONString(okUrls) + "\n";
                byte[] bytes = str.getBytes("utf-8");
                fileOutputStream.write(bytes);
                okUrls.clear();
            }

//        okUrls = okUrls.stream().distinct().collect(Collectors.toList());
//        for (String url : okUrls) {
//            if(StringUtils.isEmpty(url)){
//                continue;
//            }
//            //输出到文件
//            String str = url + "\n";
//            byte[] bytes = str.getBytes("utf-8");
//            fileOutputStream.write(bytes);
//        }

        fileOutputStream.flush();
        fileOutputStream.close();
    }
}
