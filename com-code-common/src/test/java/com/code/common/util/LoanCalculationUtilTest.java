package com.code.common.util;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2020/9/19.
 * 贷款计算测试
 */
public class LoanCalculationUtilTest {

    //贷款总额
    double total = 500000;
    //年利率
    double yearRate = 0.06;
    //月利率
    double mothRate = yearRate / 12;
    //还款期数
    int numberOfPeriods = 12 * 30;

    /**
     * 等额本息
     */
    @Test
    public void equalPrincipalAndInterestTest(){
         /*
        具体到每月的本金和利息计算：
        第一月：贷款余额28.5万元，应支付利息1317.23元（285000×5.54625%/12），应支付本金649.19元，仍欠银行贷款284350.81元；
        第二期应支付利息1314.23元（284350.81×5.54625%/12)，应支付本金652.2元，仍欠银行贷款283698.61元；
        以此类推。
         */

//        //贷款总额
//        double total = 120000;
//        //年利率
//        double yearRate = 0.06;
//        //月利率
//        double mothRate = yearRate / 12;
//        //还款期数
//        int numberOfPeriods = 12 * 1;

        double loopTotal = total;
        int loopNum = numberOfPeriods;
        double totalInterest = 0.0;
        List<Map<String, Object>> list = new ArrayList<>();
        for (int i = 1; i <= numberOfPeriods; i++) {
            //本息
            double totalMoney = LoanCalculationUtil.equalPrincipalAndInterest(loopTotal, mothRate, loopNum);
            totalMoney = BigDecimalUtl.format(totalMoney);

            //当月应还利息
            double nowInterest = loopTotal * mothRate;
            nowInterest = BigDecimalUtl.format(nowInterest);

            //当月应还本金
            double nowPrincipal = totalMoney - nowInterest;
            nowPrincipal = BigDecimalUtl.format(nowPrincipal);

            //剩余本金
            loopTotal -= nowPrincipal;
            loopTotal = BigDecimalUtl.format(loopTotal);

            //利息总和
            totalInterest += nowInterest;
            totalInterest = BigDecimalUtl.format(totalInterest);

            //期数减少1
            loopNum--;

            HashMap<String, Object> map = new HashMap<>();
            map.put("n", i);
            map.put("nowPrincipal", nowPrincipal);
            map.put("nowInterest", nowInterest);
            map.put("totalMoney", totalMoney);
            map.put("loopTotal", loopTotal);
            list.add(map);
        }

        System.out.println("【等额本息】");
        System.out.println(String.format("贷款总额 %s | 利息总计 %s", total, totalInterest));
        System.out.println("期数\t 月供本金\t 月供利息\t 月供\t 剩余本金");
        for (Map<String, Object> map : list) {
            System.out.println(String.format("%s\t %s\t %s\t %s\t %s", map.get("n"), map.get("nowPrincipal"), map.get("nowInterest"), map.get("totalMoney"), map.get("loopTotal")));
        }
    }

    /**
     * 等额本金
     */
    @Test
    public void equalPrincipalTest(){
//        //贷款总额
//        double total = 120000;
//        //年利率
//        double yearRate = 0.06;
//        //月利率
//        double mothRate = yearRate / 12;
//        //还款期数
//        int numberOfPeriods = 12 * 1;

        double loopTotal = total;
        int loopNum = numberOfPeriods;
        double totalInterest = 0.0;
        List<Map<String, Object>> list = new ArrayList<>();
        for (int i = 1; i <= numberOfPeriods; i++) {
            //本息
            double totalMoney = LoanCalculationUtil.equalPrincipal(loopTotal, mothRate, loopNum);
            totalMoney = BigDecimalUtl.format(totalMoney);

            //当月应还利息
            double nowInterest = loopTotal * mothRate;
            nowInterest = BigDecimalUtl.format(nowInterest);

            //当月应还本金
            double nowPrincipal = totalMoney - nowInterest;
            nowPrincipal = BigDecimalUtl.format(nowPrincipal);

            //剩余本金
            loopTotal -= nowPrincipal;
            loopTotal = BigDecimalUtl.format(loopTotal);

            //利息总和
            totalInterest += nowInterest;
            totalInterest = BigDecimalUtl.format(totalInterest);

            //期数减少1
            loopNum--;

            HashMap<String, Object> map = new HashMap<>();
            map.put("n", i);
            map.put("nowPrincipal", nowPrincipal);
            map.put("nowInterest", nowInterest);
            map.put("totalMoney", totalMoney);
            map.put("loopTotal", loopTotal);
            list.add(map);
        }

        System.out.println("【等额本金】");
        System.out.println(String.format("贷款总额 %s | 利息总计 %s", total, totalInterest));
        System.out.println("期数\t 月供本金\t 月供利息\t 月供\t 剩余本金");
        for (Map<String, Object> map : list) {
            System.out.println(String.format("%s\t %s\t %s\t %s\t %s", map.get("n"), map.get("nowPrincipal"), map.get("nowInterest"), map.get("totalMoney"), map.get("loopTotal")));
        }
    }
}
