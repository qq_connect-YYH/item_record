package com.code.common.util;

import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Y.YH
 * @date 2020/7/30 11:57
 * @description
 */
public class CalendarTest {
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Test
    public void obtainNDay() {
        Calendar calendar = CalendarUtil.obtainNDay(2);
        System.out.println(format.format(calendar.getTime()));
    }

    @Test
    public void obtainTodayTimeIntervalList() {
        List<Date> dates = CalendarUtil.obtainTodayTimeIntervalList();
        for (Date date : dates) {
            System.out.println(format.format(date.getTime()));
        }
    }
}
