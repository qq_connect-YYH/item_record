package com.code.common.util;

import cn.hutool.core.date.DatePattern;
import com.alibaba.fastjson.JSON;
import com.code.common.vo.TimePair;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Test;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Y.YH
 * @date 2020/11/23 11:33
 * @description
 */
@Slf4j
public class TimePairTest {

    /**
     * 根据查询日期区间，与促销日期匹配，将查询区间往前补对应的促销日期
     */
    @Test
    public void TimePairTest() throws ParseException {
        Date nowTime = new Date();
        nowTime = DateUtils.parseDate("2020-11-12 00:00:00", DatePattern.NORM_DATETIME_PATTERN);
        int intervalDay = 77;
        Date startTime = DateUtils.addDays(nowTime, -intervalDay);

        //end包含 ( ..., 促销]
//        startTime = DateUtils.formatStringToDate("2020-11-01 00:00:00", DatePattern.NORM_DATETIME_PATTERN);
//        nowTime = DateUtils.formatStringToDate("2020-11-13 13:14:00", DatePattern.NORM_DATETIME_PATTERN);
//        //被促销包含 [促销, ..., 促销]
//        startTime = DateUtils.formatStringToDate("2020-11-11 00:00:00", DatePattern.NORM_DATETIME_PATTERN);
//        nowTime = DateUtils.formatStringToDate("2020-11-12 13:15:00", DatePattern.NORM_DATETIME_PATTERN);
//        //start包含 [促销, ...)
//        startTime = DateUtils.formatStringToDate("2020-11-11 13:00:00", DatePattern.NORM_DATETIME_PATTERN);
//        nowTime = DateUtils.formatStringToDate("2020-11-16 00:00:00", DatePattern.NORM_DATETIME_PATTERN);
//        //中间包含促销,边界不重合  (..., 促销, ...)
//        startTime = DateUtils.formatStringToDate("2020-11-01 00:00:00", DatePattern.NORM_DATETIME_PATTERN);
//        nowTime = DateUtils.formatStringToDate("2020-11-16 00:00:00", DatePattern.NORM_DATETIME_PATTERN);

//        TimePair timePair = new TimePair(startTime.getTime(), nowTime.getTime(), SaleChannelConstant.ALIEXPRESS);
        TimePair timePair = new TimePair(startTime.getTime(), nowTime.getTime(), "");

        //计算促销时间
//        TimePairUtil.calcTimePair(timePair);
        TimePairUtil.calcTimePair(timePair, false);


        //总补偿时间 == 促销时间之和
        long queryTimeIncrement = timePair.getOriginStart().getTime() - timePair.getStartTM();
        long specialTimeIncrement = 0;
        List<TimePair> list = timePair.getSpecialSaleList();
        if(CollectionUtils.isNotEmpty(list)){
            specialTimeIncrement = list.stream()
                    .collect(Collectors.summingLong(o -> o.getEndTM() - o.getStartTM()));
        }

        System.out.println(String.format("queryTimeIncrement -> %s ,specialTimeIncrement -> %s , equals result: %s", queryTimeIncrement, specialTimeIncrement, (queryTimeIncrement == specialTimeIncrement)));

        log.info("{}", JSON.toJSONString(timePair));


    }
}
