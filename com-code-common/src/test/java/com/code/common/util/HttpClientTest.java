package com.code.common.util;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.lang.TypeReference;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONConverter;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.code.common.util.HttpClientUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Y.YH
 * @date 2020/1/2 12:01
 * @description
 */
@Slf4j
public class HttpClientTest {
    @Test
    public void doGetStream(){
        String url = "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1596797421604&di=3bad2949826c0a33e22938349f9a2bfd&imgtype=0&src=http%3A%2F%2Fa3.att.hudong.com%2F14%2F75%2F01300000164186121366756803686.jpg";
        InputStream inputStream = HttpClientUtil.doGetStream(url);
        try {
            File image = File.createTempFile("image", ".jpg", new File("d://"));
            FileOutputStream fileOutputStream = new FileOutputStream(image);
            byte[] buffer = new byte[1024];
            int len ;
            while ((len = inputStream.read(buffer)) != -1){
                fileOutputStream.write(buffer, 0, len);
            }
            fileOutputStream.flush();
            fileOutputStream.close();
            log.info("file {}", image.getName());
        }catch (Exception e){
            log.error("error", e);
        }
    }

    @Test
    public void doGet(){
        String url = "http://192.168.8.219:8084/sale-publish-no/xx/spuRelationTemplate/28520";
        HashMap<String, Object> param = new HashMap<>();
        param.put("a", 1);
        param.put("b", "2");
        Object o = HttpClientUtil.doGet(url, param, null);
        log.info("req val {}", o);
    }

    @Test
    public void doPostQuery(){
        String url = "http://192.168.8.219:8084/sale-publish-no/xx";
        Map<String, Object> body = new HashMap<>();
        body.put("args",  "{'search':{'spu':'','template_ids':'','successRateStart':null,'successRateEnd':null,'queryTemplateIsNull':false},'pageReqired':true,'limit':10,'offset':0}");
        body.put("method","searchAmazonAutoPublishReport");

        Object o = HttpClientUtil.doPost(url, body, null);

        log.info("val {}", o);
    }

    @Test
    public void doPostFile(){
        Map<String, Object> body = new HashMap<>();
        body.put("method","test");
        File file = new File("D:\\下载\\导入模板.xlsx");
        String url = "http://192.168.3.165:89/sale-publish-no/attrRelation/importAttrRelation/Amazon";
        Object o = HttpClientUtil.doPostFile(url, body, null, file);
        log.info("val {}", o);
    }


    @Test
    public void doPost() throws Exception {
        Map<String, Object> map = new HashMap<>();
        String picture_url = "http:xxx";
        HttpClient httpClient = HttpClientBuilder.create().build();

        // 声明httpPost请求
        HttpPost httpPost = new HttpPost(picture_url);
        httpPost.setHeader("Cookie", "xxx=xxx");// token

        // 判断map不为空
        if (map != null) {
            // 声明存放参数的List集合
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            // 遍历map，设置参数到list中
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                params.add(new BasicNameValuePair(entry.getKey(), entry.getValue().toString()));
            }
            // 创建form表单对象
            UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(params, "utf-8");
            formEntity.setContentType("application/x-www-form-urlencoded; charset=UTF-8");
            // 把表单对象设置到httpPost中
            httpPost.setEntity(formEntity);
        }
        // 使用HttpClient发起请求，返回response
        HttpResponse execute = httpClient.execute(httpPost);

        System.out.println(JSON.toJSONString(execute.getEntity()));

    }
}
