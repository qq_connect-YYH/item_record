package com.code.common.util;

import cn.hutool.core.collection.ConcurrentHashSet;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CyclicBarrier;

/**
 * @author Y.YH
 * @date 2019/12/6 18:12
 * @description
 */
public class MapUtilTest {

    /**
     * 线程安全的 putIfAbsent方法
     * @see com.code.common.util.MapUtil
     * @throws InterruptedException
     */
    @Test
    public void mapTest() throws InterruptedException {
        String key = "test";
        int count = 60;
        Map<String, List<String>> map = new ConcurrentHashMap<>();
        ConcurrentHashSet<List<String>> sets = new ConcurrentHashSet<>( );
        CyclicBarrier cyclicBarrier = new CyclicBarrier(count, () ->{
            System.out.println("一起执行了.......");
        });
        while (true){
            if(count-- <= 0){
                break;
            }
            new Thread(() ->{
                try {
                    cyclicBarrier.await();
                    //阻塞到一起请求，看是否线程安全
                    List<String> list = MapUtil.putIfAbsent(map, key, () -> {
                        return new ArrayList<>();
                    });
                    //同一个地址即相同
                    sets.add(list);
                }catch (Exception e){
                    e.printStackTrace();
                } finally {
                }
            }).start();
        }
        Thread.sleep(1000 * 10);
        System.out.println("sets size-> "+ sets.size());
        for (List<String> set : sets) {
            System.out.println(set);
        }
    }


}
