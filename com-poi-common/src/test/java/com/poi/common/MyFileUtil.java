package com.poi.common;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Y.YH
 * @date 2020/8/15 11:15
 * @description
 */
public class MyFileUtil {


//    public static ConcurrentLinkedQueue<String> waitOutList = new ConcurrentLinkedQueue<>();
//    private static final Timer TIMER = new Timer();
//    static {
//        TIMER.scheduleAtFixedRate(new TimerTask() {
//            @Override
//            public void run() {
//                if(waitOutList.size() >0){
//                    File file = new File("d://successAccount.txt");
//                    try {
//                        if(!file.exists()){
//                            file.createNewFile();
//                        }
//                        RandomAccessFile writer = new RandomAccessFile(file, "rw");
//                        FileChannel channel = writer.getChannel();
//                        StringBuilder str = new StringBuilder();
//                        while (waitOutList.size() >0){
//                            String poll = waitOutList.poll();
//                            str.append(poll +"\n");
//                        }
//                        ByteBuffer buff = ByteBuffer.wrap(str.toString().getBytes(StandardCharsets.UTF_8));
//                        channel.write(buff);
//                        writer.close();
//                    }catch (Exception e){}
//                }
//
//            }
//        }, 60 * 1000, 1 * 60 * 1000);
//    }

    public static void file2Excel( List<String> list, String accountNumber) throws IOException {
        List<String[]> lineSplits = list.subList(1, list.size()).stream()
                .filter(row -> StringUtils.isNotBlank(row) && !StringUtils.contains(row, "merchant-shipping-group"))
                .map(row -> StringUtils.splitPreserveAllTokens(row, "\t"))
                .collect(Collectors.toList());
        List<String> headers = Arrays.asList(list.get(0).split("\t"));

        Workbook wb = new XSSFWorkbook();

        Sheet sheet = wb.createSheet(String.format("第%s页", 1));
        createSheetHeader(wb, sheet, headers);
        Row row = null;
        int rowIndex = 0;
        for (String[] item : lineSplits) {
            row = sheet.createRow(++rowIndex);
            for (int cellIndex = 0; cellIndex < item.length; cellIndex++) {
                row.createCell(cellIndex).setCellValue(item[cellIndex]);
            }
        }
        File test = File.createTempFile(accountNumber +" ", ".xlsx", new File("d://"));
        FileOutputStream fileOutputStream = new FileOutputStream(test);
        wb.write(fileOutputStream);
    }
    private static void createSheetHeader(Workbook wb, Sheet sheet, List<String> headers) {
        if (null == sheet) {
            return;
        }

        Row row = sheet.createRow(0);
        CellStyle style = wb.createCellStyle();
        style.setAlignment(XSSFCellStyle.ALIGN_CENTER);

        for (int i = 0; i < headers.size(); i++) {
            Cell cell = row.createCell(i);
            cell.setCellValue(headers.get(i));
            cell.setCellStyle(style);
        }
    }
}
