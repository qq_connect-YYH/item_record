package com.yyh.item.codegen;

import org.apache.commons.configuration.Configuration;

import java.sql.*;

/**
 * Created by Administrator on 2019/9/14.
 */
public class ConnectionUtil {

    /**
     * 学习：https://www.cnblogs.com/wgl-gdyuan/p/9906722.html
     *
     * Oracle：jdbc:oracle:thin:@localhost:1521:sid
     * SqlServer：jdbc:microsoft:sqlserver://localhost:1433; DatabaseName=sid
     * Mysql：jdbc:mysql://localhost:3306/test?useSSL=false
     * @return
     */
    public static Connection getConnection(){
        try{
            //加载数据库驱动
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("数据库驱动加载成功");
            String url="jdbc:mysql://localhost:3306/test?useSSL=false";
            //如果不加useSSL=false就会有警告，由于jdbc和mysql版本不同，有一个连接安全问题
            String user="root";
            String passWord="root123456";
            //Connection对象引的是java.sql.Connection包
            //创建连接
            Connection connection = DriverManager.getConnection(url, user, passWord);
            System.out.println("已成功的与数据库MySQL建立连接！！");
            return connection;
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static Connection getConnection(Configuration configuration) {
        try{
            String driverClass = configuration.getString("driverClass");
            String url = configuration.getString("url");
            String username = configuration.getString("username");
            String password = configuration.getString("password");
            //加载数据库驱动
            Class.forName(driverClass);
            //创建连接
            Connection connection = DriverManager.getConnection(url, username, password);
            System.out.println("已成功的与数据库MySQL建立连接！！");
            return connection;
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static void releaseDB(Connection connection, PreparedStatement preparedStatement, ResultSet resultSet) {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (preparedStatement != null) {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


}


