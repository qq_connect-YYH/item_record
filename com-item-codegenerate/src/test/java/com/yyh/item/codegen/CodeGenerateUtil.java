package com.yyh.item.codegen;

import com.yyh.item.codegen.entity.GenConfig;
import com.yyh.item.codegen.service.SysGeneratorService;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by Administrator on 2019/11/30.
 */

@SpringBootTest(classes = Application.class)
@RunWith(SpringRunner.class)
public class CodeGenerateUtil {

    @Autowired
    SysGeneratorService sysGeneratorService;

    /**
     * 生成代码到桌面
     * @throws Exception
     */
    @Test
    public void codeGenerateToDesktop() throws Exception {
        Configuration configuration = new PropertiesConfiguration("generator.properties");
        GenConfig genConfig = new GenConfig();
        genConfig.setDatabase(configuration.getString("database"));
        genConfig.setTableName(configuration.getString("tableName"));
        byte[] data = sysGeneratorService.generatorCode(genConfig);

        FileSystemView fsv = FileSystemView.getFileSystemView();
        File com = fsv.getHomeDirectory();
        File filePath = new File(com.getPath());

        File file = File.createTempFile(genConfig.getTableName(), ".zip", filePath);
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(data);
    }

    @Test
    public void codeGenerateToProduct() throws Exception {
        Configuration configuration = new PropertiesConfiguration("generator.properties");
        GenConfig genConfig = new GenConfig();
        genConfig.setDatabase(configuration.getString("database"));
        genConfig.setTableName(configuration.getString("tableName"));
        sysGeneratorService.generatorCode(genConfig, true);
    }
}
