package com.yyh.item.codegen;

import com.alibaba.fastjson.JSON;
import com.mysql.cj.jdbc.result.ResultSetImpl;
import com.mysql.cj.protocol.ColumnDefinition;
import com.mysql.cj.protocol.ResultsetRows;
import com.mysql.cj.protocol.a.result.ByteArrayRow;
import com.mysql.cj.result.Field;
import com.mysql.cj.result.Row;
import com.mysql.cj.result.StringValueFactory;
import com.yyh.item.codegen.entity.GenConfig;
import com.yyh.item.codegen.util.GenUtils;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2019/12/22.
 */
public class CodeGenerateUtilCustom {

    //查询数据库对应的表名
    private static String queryTableSql = "select table_name tableName, engine, table_comment tableComment from information_schema.tables where table_schema = ? and table_name = ? ";
    //查询表对应的字段
    private static String queryTableColumnSql = "select column_name columnName, data_type dataType, column_comment columnComment, column_key columnKey, extra from information_schema.columns where table_schema = ? and table_name = ? ";

    /**
     * 自定义数据源，手动执行生成代码
     * @throws Exception
     */
    @Test
    public void codeGenerate() throws Exception {
        Configuration configuration = new PropertiesConfiguration("generator.properties");
        GenConfig genConfig = new GenConfig();
        genConfig.setDatabase(configuration.getString("database"));
        genConfig.setTableName(configuration.getString("tableName"));

        PreparedStatement preparedStatement = null;
        ResultSetImpl resultSet = null;
        Connection connection = null;
        try {
            connection = ConnectionUtil.getConnection(configuration);

            //查询表
            preparedStatement = connection.prepareStatement(queryTableSql);
            preparedStatement.setString(1, genConfig.getDatabase());
            preparedStatement.setString(2, genConfig.getTableName());
            resultSet = (ResultSetImpl) preparedStatement.executeQuery();

            HashMap<String, String> tableResultMap = new HashMap<>();
            ResultsetRows rows = resultSet.getRows();
            ColumnDefinition metadata = rows.getMetadata();
            Field[] fields = metadata.getFields();
            while (rows.hasNext()){
                Row row = rows.next();
                for (int i = 0; i < fields.length; i++) {
                    Field field = fields[i];
                    String key = field.getColumnLabel();
                    String value = row.getValue(i, new StringValueFactory());
                    tableResultMap.put(key, value);
                }
            }
            System.out.println(JSON.toJSONString(tableResultMap));

            //查询字段
            List<Map<String, String>> columnsList = new ArrayList<>();
            preparedStatement = connection.prepareStatement(queryTableColumnSql);
            preparedStatement.setString(1, genConfig.getDatabase());
            preparedStatement.setString(2, genConfig.getTableName());
            resultSet = (ResultSetImpl) preparedStatement.executeQuery();
            rows = resultSet.getRows();
            metadata = rows.getMetadata();
            fields = metadata.getFields();
            while (rows.hasNext()){
                Row row =  rows.next();
                Map<String, String> rowMap = new HashMap<>(rows.size());
                for (int i = 0; i < fields.length; i++) {
                    Field field = fields[i];
                    String key = field.getColumnLabel();
                    String value = row.getValue(i, new StringValueFactory());
                    rowMap.put(key, value);
                }
                columnsList.add(rowMap);
            }
            System.out.println(JSON.toJSONString(columnsList));
            //生成代码
            GenUtils.generatorCode(genConfig, tableResultMap, columnsList);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            ConnectionUtil.releaseDB(connection, preparedStatement, resultSet);
        }


    }


}
