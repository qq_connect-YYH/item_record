package com.yyh.item.codegen.util;

import com.yyh.item.codegen.constant.CommonConstant;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Builder
@ToString
@Accessors(chain = true)
@AllArgsConstructor
public class R<T> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	private int code = CommonConstant.SUCCESS;

	@Getter
	@Setter
	private String msg = "success";


	@Getter
	@Setter
	private T data;

	public R() {
		super();
	}

	public R(T data) {
		super();
		this.data = data;
	}

	public R(T data, String msg) {
		super();
		this.data = data;
		this.msg = msg;
	}

	public R(Throwable e) {
		super();
		this.msg = e.getMessage();
		this.code = CommonConstant.FAIL;
	}

	public static R success() {
		return new R();
	}

	public static R success(Object data) {
		R success = success();
		success.setData(data);
		return success;
	}
	public static R success(String msg, Object data) {
		R success = success(data);
		success.setMsg(msg);
		return success;
	}

	public static R fall(String msg) {
		R r = new R();
		r.setCode(1);
		r.setMsg(msg);
		return r;
	}

}
