package com.yyh.item.codegen.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yyh.item.codegen.entity.GenConfig;

import java.util.List;
import java.util.Map;

public interface SysGeneratorService {
	/**
	 * 生成代码
	 *
	 * @param tableNames 表名称
	 * @return
	 */
	byte[] generatorCode(GenConfig tableNames);

	/**
	 *
	 * @param tableNames
	 * @param genToProduct 是否生成在项目里面 true：是 ，false：否（生成到桌面）
	 * @return
	 */
	byte[] generatorCode(GenConfig tableNames, boolean genToProduct);

	
	/**
	 * 查询数据库列表
	 * @return
	 */
	List<Map<String, String>> getDatabaseList();

	/**
	 * 分页查询表
	 * @param database 数据库名
	 * @param tableName 表名
	 * @return
	 */
	IPage<List<Map<String, Object>>> getPage(Page page, String database, String tableName);
}
