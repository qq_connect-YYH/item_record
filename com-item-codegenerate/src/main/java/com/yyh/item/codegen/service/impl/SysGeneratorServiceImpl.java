package com.yyh.item.codegen.service.impl;

import cn.hutool.core.io.IoUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yyh.item.codegen.entity.GenConfig;
import com.yyh.item.codegen.mapper.SysGeneratorMapper;
import com.yyh.item.codegen.service.SysGeneratorService;
import com.yyh.item.codegen.util.GenUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;

@Service
@AllArgsConstructor
public class SysGeneratorServiceImpl implements SysGeneratorService {
	private final SysGeneratorMapper sysGeneratorMapper;


	/**
	 * 分页查询表
	 *
	 * @param tableName 查询条件
	 * @return
	 */
	@Override
	public IPage<List<Map<String, Object>>> getPage(Page page,String database, String tableName) {
		return sysGeneratorMapper.queryList(page,database,tableName);
	}

	/**
	 * 生成代码
	 *
	 * @param genConfig 生成配置
	 * @return
	 */
	@Override
	public byte[] generatorCode(GenConfig genConfig) {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		ZipOutputStream zip = new ZipOutputStream(outputStream);

		//查询表信息
		Map<String, String> table = queryTable(genConfig.getDatabase(), genConfig.getTableName());
		//查询列信息
		List<Map<String, String>> columns = queryColumns(genConfig.getDatabase(),genConfig.getTableName());
		//生成代码
		GenUtils.generatorCode(genConfig, table, columns, zip);
		IoUtil.close(zip);
		return outputStream.toByteArray();
	}

	@Override
	public byte[] generatorCode(GenConfig genConfig, boolean genToProduct) {
		//查询表信息
		Map<String, String> table = queryTable(genConfig.getDatabase(), genConfig.getTableName());
		//查询列信息
		List<Map<String, String>> columns = queryColumns(genConfig.getDatabase(),genConfig.getTableName());

		//生成代码
		GenUtils.generatorCode(genConfig, table, columns);
		return null;
	}

	private Map<String, String> queryTable(String database, String tableName) {
		return sysGeneratorMapper.queryTable(database,tableName);
	}

	private List<Map<String, String>> queryColumns(String database,String tableName) {
		return sysGeneratorMapper.queryColumns(database,tableName);
	}

	@Override
	public List<Map<String, String>> getDatabaseList() {
		return sysGeneratorMapper.queryDatabase();
	}
}
